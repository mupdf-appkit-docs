.. include:: header.rst



Document Setup
==================



Setup
-----------

PDF documents in App Kit are always rendered inside a document view controller instance.

Depending on your integration your application should use one of the following document view controllers:

- `DefaultUIViewController` ( for the :ref:`Default UI<iOS_DefaultUI>` )

- `MuPDFDKBasicDocumentViewController` ( for a :ref:`Custom UI<iOS_CustomUI>` )

The :ref:`Default UI<iOS_DefaultUI>` involves the least effort and should handle all your requirements unless you need nontrivial customization, more than altering colors and icons within the UI. Simply create and present a `DefaultUIViewController` instance. By using the :ref:`Default UI<iOS_DefaultUI>`, all user editing features are handled internally within the instance, without you as application programmer having to interact with the process.

For the :ref:`Custom UI<iOS_CustomUI>`, a `MuPDFDKBasicDocumentViewController` instance provides a view on the document, but with minimal UI. It is the application developer's responsibility to create additional views and menus with UI elements via which the user can perform operations on the document. An application developer would typically create a `UIViewController` subclass that acts as a container for a `MuPDFDKBasicDocumentViewController` instance, along with additional views for UI. The `UIViewController` subclass would respond to the regular view-related `UIViewController` events in the usual way, and additionally implement some further protocols to make the UI responsive to events relating to the document and its view:

- :ref:`ARDKBasicDocViewDelegate<iOS_ARDKBasicDocViewDelegate>` - this allows the document view to inform us of events and request information, regarding scrolling, user taps, etc.

- :ref:`ARDKDocumentEventTarget<iOS_ARDKDocumentEventTarget>` - this allows the document itself to inform us of events, regarding the loading of the document and selection changes etc.

- :ref:`ARDKPageSelectorDelegate<iOS_ARDKPageSelectorDelegate>` - this allows to listen for page events such as selecting, deleting, duplicating or moving pages.



View Controller Interfaces
-------------------------------

Security setup
~~~~~~~~~~~~~~~~~~~

There are four optional interfaces that can be implemented via your own custom classes in order to define how App Kit manages data security.

- ARDKPasteboard_
- ARDKSettings_
- ARDKFileState_
- ARDKSecureFS_


.. note::

    There are no defaults for these interfaces.

These interfaces are specifically important when considering a :ref:`Custom UI<iOS_CustomUI>` approach.


ARDKPasteboard
"""""""""""""""""

An interface that specifies the basis for implementing a class to control pasteboard copy and paste information.

ARDKSettings
"""""""""""""""""

An interface that specifies the basis for implementing a class to set the location of where temporary files are stored.

ARDKFileState
"""""""""""""""""

An interface that specifies the basis for implementing a class to provide information about the file being opened.

ARDKSecureFS
"""""""""""""""""

An interface that specifies the basis for implementing a class to allow proprietary encrypted files, stored in a secure container. A developer can use this opportunity to enforce security and role restrictions, or map the file operations onto another mechanism, such as a database.



.. include:: footer.rst