.. include:: header.rst



PDF Redactions
==================

.. raw:: html

    <div class="appKitProOnlyMessage"></div><br>


Redactions
-------------------

The redaction feature has been designed to work on text, images, and links. By selecting all or part of an image, text, or link, and applying redaction this then permanently redacts the selected information, making it impossible to retrieve the original data.

Redacted text and areas can be :ref:`marked<Android_Redactions_Mark>`, and any marked redactions can be :ref:`removed<Android_Redactions_Remove>`, however once the redactions are :ref:`applied<Android_Redactions_Apply>` and the document is saved then the redacted information is blocked out with black to denote the area of redaction. At this point the original text and/or area is unretrievable.





.. _Android_Redactions_Mark:

Marking Text for Redaction
--------------------------------------

To mark text for redaction call the `redactMarkText()` method against the `DocumentView` instance. The document's currently selected text will then be marked with a red-outlined box to denote the redaction demarcation.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.redactMarkText()

   .. tab:: Java

      .. code-block:: java

        documentView.redactMarkText();



Marking an Area for Redaction
----------------------------------

To mark an area for redaction call the `redactMarkArea()` method against the `DocumentView` instance. The document will then enter a mode whereby the user can then draw a red-outlined boxed area to denote the redaction demarcation.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.redactMarkArea()

   .. tab:: Java

      .. code-block:: java

        documentView.redactMarkArea();


.. _Android_Redactions_Remove:

Removing a Marked Redaction
----------------------------------

To remove a marked redaction call the `redactRemove()` method against the `DocumentView` instance. The document's currently selected marked for redaction ( i.e. a selection of text or an area with the red box outline ) will then no longer be marked for redaction.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.redactRemove()

   .. tab:: Java

      .. code-block:: java

        documentView.redactRemove();



.. _Android_Redactions_Apply:


Applying Redactions
----------------------------------

Applying redactions means that all document selections which are marked for redaction will be redacted. To apply redactions call the `redactApply()` method against the `DocumentView` instance. Once redactions have been applied then the redacted text is blocked out with black and the red redaction demarcation areas are removed.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.redactApply()

   .. tab:: Java

      .. code-block:: java

        documentView.redactApply();


.. note::

    Redactions are not permanently set until the document is saved. If the document is exited without saving then the redactions will be lost.


.. include:: footer.rst