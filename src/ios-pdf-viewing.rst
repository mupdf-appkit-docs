.. include:: header.rst



PDF Viewing
==================



Present a Document View
---------------------------

There are two fundamental ways of presenting a document to the screen. One way is to use the :ref:`Default UI<iOS_DefaultUI>` which includes a user interface. The alternative is to load the document into a dedicated view controller and provide your own :ref:`Custom UI<iOS_CustomUI>` with delegate methods available for your document control.


.. _iOS_DefaultUI:

Default UI
---------------

The :ref:`Default UI<iOS_DefaultUI>` is an App Kit UI created by Artifex which includes a user-interface for typical document features and actions. It is presented at the top of the document view and accommodates for both tablet and phone layout.

The :ref:`Default UI<iOS_DefaultUI>` aims to deliver a handy way of allowing for document viewing & manipulation without the need to provide your own :ref:`Custom UI<iOS_CustomUI>`.



.. _iOS_PDF_Viewing_Basic_Usage:

Basic Usage
~~~~~~~~~~~~~~~~~~

Assuming that your view controller has a navigation controller then local documents in the iOS main bundle can be presented by pushing an instance of `DefaultUIViewController` from your dedicated `UIViewController` as follows:



.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            import mupdfdk
            import mupdf_default_ui
            ...

            let documentPath:String = "sample-document.pdf"

            if let vc = DefaultUIViewController.viewController(path: documentPath) {
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }


    .. tab:: Objective-C

       .. code-block:: obj-c


            #import "mupdfdk/mupdfdk.h"
            #import "mupdf_default_ui/mupdf_default_ui.h"
            ...

            NSString *documentPath = @"sample-document.pdf";
            DefaultUIViewController *vc = [DefaultUIViewController viewControllerWithPath:documentPath];
            [self.navigationController pushViewController:vc animated:YES];




To load files via a defined URL use the following:


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            if let vc = DefaultUIViewController.viewController(url: url) {
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }


    .. tab:: Objective-C

       .. code-block:: obj-c

            DefaultUIViewController *vc = [DefaultUIViewController viewControllerWithURL:url];
            [self.navigationController pushViewController:vc animated:YES];



.. note::

    Files outside of your App bundle will need to be opened using the :ref:`advanced method<iOS_PDF_Viewing_Advanced_Usage>`.


.. _iOS_PDF_Viewing_Advanced_Usage:


Advanced usage
~~~~~~~~~~~~~~~~

Alternatively, an application developer can open a document with a session. This enables more control with regard to the file operations and settings for the document.

Opening a document within a session involves the following:

1) Initialize the MuPDF library:


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            let mupdfdkLib:MuPDFDKLib = MuPDFDKLib.init(settings: ARDKSettings())

    .. tab:: Objective-C

       .. code-block:: obj-c

            MuPDFDKLib *mupdfdkLib = [[MuPDFDKLib alloc] initWithSettings:[[ARDKSettings alloc] init]];




2) Initialize your own :ref:`FileState<iOS_FileOperations_FileState>` class (ensuring to set the correct file path within it):


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            let fileState:MyFileState = MyFileState()

    .. tab:: Objective-C

       .. code-block:: obj-c

            MyFileState *fileState = [[MyFileState alloc] init];




3) Initialize document settings with your required `Configuration Options`:


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            let docSettings:ARDKDocumentSettings = ARDKDocumentSettings()
            docSettings.enableAll(true)

    .. tab:: Objective-C

       .. code-block:: obj-c

            ARDKDocumentSettings *docSettings = [[ARDKDocumentSettings alloc] init];
            [docSettings enableAll:YES];



4) Initialize the document session, `ARDKDocSession`, with your file state, MuPDF library and document settings instances, also set your signing delegate as required:


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            let session:ARDKDocSession = ARDKDocSession(fileState:fileState,
                                                      ardkLib:self.mupdfdkLib,
                                                  docSettings:docSettings)
            session.signingDelegate = ARDKOpenSSLSigningDelegate()

    .. tab:: Objective-C

       .. code-block:: obj-c

            ARDKDocSession *session = [ARDKDocSession sessionForFileState:fileState
                                                                  ardkLib:self.mupdfdkLib
                                                              docSettings:docSettings];
            session.signingDelegate = [[ARDKOpenSSLSigningDelegate alloc] init];



5) Instantiate the `DefaultUIViewController` with the session:


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            let vc:DefaultUIViewController = DefaultUIViewController.viewController(for: session)


    .. tab:: Objective-C

       .. code-block:: obj-c

            DefaultUIViewController *vc = [DefaultUIViewController viewControllerWithSession:session];



The Back Button
~~~~~~~~~~~~~~~~~~~~

Your dedicated view controller, which pushes the instance of `ARDKDocumentViewController`, must include one bespoke method to enable a graceful exit back from the :ref:`Default UI<iOS_DefaultUI>`. This method is detailed as follows:


.. tabs::

   .. tab:: Swift

      .. code-block:: swift

            @IBAction func docCloseUnwindAction(_ sender: UIStoryboardSegue) {}

   .. tab:: Objective-C

      .. code-block:: obj-c

            - (IBAction)docCloseUnwindAction:(UIStoryboardSegue *)sender {}



This method is triggered by the user pressing the 'back' button in the MuPDF UI.

.. note::

    The implementation can be left empty, but this function must be present in the view controller that should be unwound back to - otherwise nothing will happen when the user taps the back button.


Configuration Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~


When using the :ref:`Default UI<iOS_DefaultUI>` an application developer can optionally set certain configurable features.

The available settings conform to the `ARDKDocumentSettings` protocol which contains the following key/value pairs:


.. raw:: html

    <table>
    <thead>
    <tr>
    <th width=300px>Key</th>
    <th>Value type</th>
    </tr>
    </thead>
    <tbody><tr>
    <td>contentDarkModeEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>editingEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>fullScreenModeEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>insertFromCameraEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>insertFromPhotosEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>openInEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>openUrlEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfAnnotationsEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormFillingAvailable</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormFillingEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormSigningEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfRedactionAvailable</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfRedactionEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfSignatureFieldCreationEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfSecureRedactionAvailable</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfSecureRedactionEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormESigningAvailable</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormESigningEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormDigitalSigningAvailable</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>pdfFormDigitalSigningEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>printingEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>saveAsEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>saveButtonEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>saveToButtonEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>securePrintingEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>shareEnabled</td>
    <td><code>bool</code></td>
    </tr>
    <tr>
    <td>systemPasteboardEnabled</td>
    <td><code>bool</code></td>
    </tr>
    </tbody></table>



.. note::

    The configuration options should be set before a document is opened


To use MuPDF with configuration options an application developer should use load documents using the :ref:`document session<iOS_PDF_Viewing_Advanced_Usage>` method.


.. _iOS_CustomUI:

Custom UI
------------

Your dedicated view controller should implement the following protocols `ARDKBasicDocViewDelegate` & `ARDKDocumentEventTarget`. For more on these protocols see the :ref:`Document API<iOS_Document_API>` page.



.. tabs::

   .. tab:: Swift

      .. code-block:: swift

            import mupdfdk

            class DocumentViewController: UIViewController,
                                          ARDKBasicDocViewDelegate,
                                          ARDKDocumentEventTarget

   .. tab:: Objective-C

      .. code-block:: obj-c

            #import "mupdfdk/mupdfdk.h"

            @interface DocumentViewController() <ARDKBasicDocViewDelegate,
                                                 ARDKDocumentEventTarget>

            @end



These protocols will allow your `UIViewController` subclass to respond to events whilst presenting & interacting with the document.

There are several ways your `UIViewController` subclass can act as a container for an instance of `MuPDFDKBasicDocumentViewController`, along with UI related views. A layout described via a storyboard is one option, or a programmatic approach, as follows:


.. tabs::

   .. tab:: Swift

      .. code-block:: swift

            let docVc:MuPDFDKBasicDocumentViewController =
            MuPDFDKBasicDocumentViewController.init(forPath:documentPath)
            docVc.delegate = self
            docVc.session.doc.add(self)
            self.addChild(docVc)
            docVc.view.frame = self.view.bounds
            self.view.addSubview(docVc.view)
            docVc.didMove(toParent:self)


   .. tab:: Objective-C

      .. code-block:: obj-c

            MuPDFDKBasicDocumentViewController *docVc =
            [MuPDFDKBasicDocumentViewController viewControllerForPath:documentPath];
            docVc.delegate = self;
            [docVc.session.doc addTarget:self];
            [self addChildViewController:docVc];
            docVc.view.frame = self.view.bounds;
            [self.view addSubview:docVc.view];
            [docVc didMoveToParentViewController:self];


Going to a Page
~~~~~~~~~~~~~~~~~~~

Once a document is loaded an application developer can view pages either by scrolling the document view or by using the App Kit API as follows:

.. tabs::

   .. tab:: Swift

      .. code-block:: swift

            // note: page number is zero-indexed, thus this would show page 8 of your document
            documentViewController.showPage(7)

   .. tab:: Objective-C

      .. code-block:: obj-c

            // note: page number is zero-indexed, thus this would show page 8 of your document
            [documentViewController showPage:7];



In the code sample above `documentViewController` refers to the instance of your `MuPDFDKBasicDocumentViewController`. Furthermore this API should only be called after the document has initially loaded and had it's first render (see :ref:`Document Listeners - Document completed<iOS_DocumentAPI_Document_Completed>`).

Viewing Full-Screen
~~~~~~~~~~~~~~~~~~~~~~

In order to view a document in full-screen, it is up to the application developer to hide any UI which they have present and set the frame of the document view to fill the screen. Once in full-screen, you can use :ref:`Tap selections<iOS_DocumentAPI_Swallowing_Tap_Selections>` to exit and return from the full-screen frame to any previous UI.

Document Page Viewer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is a drop-in UI available to enable a page viewer for your document which understands how to display a column of page thumbnails from information obtained from within the document session object. In order to use this UI, instantiate the `ARDKPagesViewController` class with the document session and add it to your UI as required.


.. tabs::

   .. tab:: Swift

      .. code-block:: swift

            let pagesVC = ARDKPagesViewController(session: session)
            pagesVC.view.frame = CGRect
            pagesVC.didMove(toParent: self)
            pagesVC.delegate = self
            pagesVC.selectPage(0)
            self.addChild(pagesVC)
            view.addSubview(pagesVC.view)


   .. tab:: Objective-C

      .. code-block:: obj-c

            ARDKPagesViewController *pagesVC = [ARDKPagesViewController viewControllerWithSession:self.session];
            pagesVC.view.frame = CGRect;
            [pagesVC didMoveToParentViewController:self];
            pagesVC.delegate = self;
            [pagesVC selectPage:0];
            [self addChildViewController:pagesVC];
            [view addSubview:pagesVC.view];


.. note::

    The delegate protocol for `ARDKPagesViewController` is :ref:`ARDKPageSelectorDelegate<iOS_ARDKPageSelectorDelegate>`.

.. include:: footer.rst