.. include:: header.rst






Quick Start Guide
=====================


:title:`MuPDF App Kit` libraries enable quick and easy :title:`PDF` document viewing and editing for :title:`Android` and :title:`iOS` platforms.

This guide demonstrates how our :title:`Drop-in UI` solution allows you to get up and running with minimal coding effort.


.. image:: images/guide-illustration.svg
   :align: center



.. note::

   The :title:`Drop-in UI` connects with the native file browser on your :title:`Android` and :title:`iOS` platforms for ease of integration and to enable the best possible document management experience.


Setup & integration
-----------------------


Android
~~~~~~~~~~~~~~~



1. Embed the :title:`App Kit` libraries into your project folder:

.. image:: images/android/android-libs.png
   :align: center
   :scale: 70%


2. Set the minimum :title:`Android SDK` level in your :title:`Gradle` file:


.. code-block:: groovy

   android {
       defaultConfig {
           minSdkVersion 23
           ...
       }
       ...
   }


3. Reference the following dependencies in your :title:`Gradle` file:

.. code-block:: groovy

   dependencies {
       ...
       // Dependency on view binding
       implementation 'androidx.databinding:viewbinding:7.1.2'
       // Dependency on navigation fragments
       implementation 'androidx.navigation:navigation-fragment-ktx:2.2.2'
       implementation 'androidx.navigation:navigation-ui-ktx:2.2.2'

       // Dependency on local binaries
       implementation fileTree(dir: 'libs', include: ['*.aar'])
   }

4. Include the default UI activity in your `AndroidManifest.xml` as follows:


.. code-block:: xml

   <activity android:name="com.artifex.sonui.editor.default_ui.DefaultUIActivity"
       android:exported="true"
       android:configChanges="orientation|keyboard|keyboardHidden|screenSize|smallestScreenSize|screenLayout|uiMode"
       android:screenOrientation="fullSensor"
       android:theme="@style/sodk_editor_mui_theme">
   </activity>


5. Import the default UI activity into your application code:

.. code-block:: java

   import com.artifex.sonui.editor.default_ui.DefaultUIActivity

6. Instantiate the App Kit `DefaultUIActivity` class with data representing your file `URI` and start the activity:




.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

         val defaultUI = Intent(this, DefaultUIActivity::class.java).apply {
             this.action = Intent.ACTION_VIEW
             this.data = uri
         }

         startActivity(defaultUI)


   .. tab:: Java

      .. code-block:: java

         Intent defaultUI = new Intent(this, DefaultUIActivity.class);
         defaultUI.setAction(Intent.ACTION_VIEW);
         defaultUI.setData(uri);

         startActivity(defaultUI);



.. note::

   If you send `null` for the data uri value then the system file browser will open, whereby you can select a file to view.


iOS
~~~~~~

1. Embed the :title:`App Kit` frameworks into your :title:`Xcode` project:


.. image:: images/ios/xcode-frameworks.png
   :align: center
   :scale: 70%


2. Import the frameworks into your application code:


.. tabs::

   .. tab:: Swift

      .. code-block:: swift

         import mupdfdk
         import mupdf_default_ui



   .. tab:: Objective-C

      .. code-block:: obj-c

         #import "mupdfdk/mupdfdk.h"
         #import "mupdf_default_ui/mupdf_default_ui.h"



3. Instantiate the :title:`App Kit` `DefaultUIViewController` with your document `URL` and push it to your navigation controller:


.. tabs::

   .. tab:: Swift

      .. code-block:: swift

         if let vc = DefaultUIViewController.viewController(url: url) {
             vc.modalPresentationStyle = .fullScreen
             self.navigationController?.pushViewController(vc, animated: true)
         }



   .. tab:: Objective-C

      .. code-block:: obj-c

         DefaultUIViewController *vc = [DefaultUIViewController viewControllerWithURL:url];
         if (vc != nil) {
             [self.navigationController pushViewController:vc animated:YES];
         }








Customization
------------------

If you wish to build your own UI for file viewing then you should integrate your application code with the :ref:`App Kit API<MuPDF_App_Kit_API>`.


.. include:: footer.rst
