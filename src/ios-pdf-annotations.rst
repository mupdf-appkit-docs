.. include:: header.rst



PDF Annotations
==================



Annotations
-----------------

When developing your own custom UI, you may wish to support the creation of annotations, including :ref:`drawing<iOS_Annotations_Draw_Mode>`, :ref:`adding notes<iOS_Annotations_Note_Mode>` and :ref:`highlighting<iOS_Annotations_Highlighting>` text on a PDF document.



.. _iOS_Annotations_Draw_Mode:

Draw Mode
-----------------

Turning on drawing is as simple as setting the annotating mode to `MuPDFDKAnnotatingMode_Draw` against your `MuPDFDKBasicDocumentViewController` instance. To turn off drawing mode just set the annotating mode to `MuPDFDKAnnotatingMode_None` against your `MuPDFDKBasicDocumentViewController` instance.

The following code toggles draw mode on and off and might serve as the handler for one of the buttons in your UI.



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            if basicDocVc.annotatingMode == MuPDFDKAnnotatingMode_Draw {
                basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_None
            } else {
                basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Draw
            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            if (basicDocVc.annotatingMode == MuPDFDKAnnotatingMode_Draw) {
                basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_None;
            }
            else {
                basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Draw;
            }




When `Draw Mode`_ is enabled, the user can draw an ink annotation with the selected line thickness and color. When `Draw Mode`_ is then disabled, the annotation is saved to the document.


Line Thickness
~~~~~~~~~~~~~~~~~~~~~~~

To set & get line thickness, access the `inkAnnotationThickness` value as a `CGFloat`.

.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.inkAnnotationThickness = 10



    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.inkAnnotationThickness = 10;



Line Color
~~~~~~~~~~~~~~~~~~~~~~~

To set & get line color, access the `inkAnnotationColor` value as a `UIColor`.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.inkAnnotationColor = .green


    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.inkAnnotationColor = [UIColor green];

.. _iOS_Annotations_Note_Mode:

Note Mode
------------

To turn on note mode set the annotating mode to `MuPDFDKAnnotatingMode_Note` against your `MuPDFDKBasicDocumentViewController` instance. To save the note set the annotating mode to `MuPDFDKAnnotatingMode_None`.

.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Note

    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Note;


.. _iOS_Annotations_Highlighting:


Highlighting
-----------------

To turn on highlighting set the annotating mode to `MuPDFDKAnnotatingMode_HighlightTextSelect` against your `MuPDFDKBasicDocumentViewController` instance. When in text-highlighting mode, the document view will allow the user to create a text highlight by dragging across text, whereupon the mode will revert back to `MuPDFDKAnnotatingMode_None`, leaving a newly created annotation selected. The selected annotation will show drag handles which the user can use to adjust it.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_HighlightTextSelect

    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_HighlightTextSelect;


.. note::

    Highlight mode will only affect the actively selected text before the mode is activated.


De-selecting Annotations
----------------------------

Essentially all annotations are treated the same as simply selections once they have been selected by a user. This selection can then be cleared (de-selected) by calling the `clearSelection()` method against the `MuPDFDKDoc` instance within `MuPDFDKBasicDocumentViewController`.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let myDoc:MuPDFDKDoc? = basicDocVc?.session.doc as! MuPDFDKDoc
            myDoc?.clearSelection()

    .. tab:: Objective-C

        .. code-block:: obj-c

            MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
            [myDoc clearSelection];



Without an annotation being currently selected, calling `clearSelection()` will have no effect.



Deleting Annotations
----------------------------

Selections can be removed (deleted) by calling the `deleteSelectedAnnotation()` method against the `MuPDFDKDoc` instance within `MuPDFDKBasicDocumentViewController`.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func deleteDocumentSelection() {
                let myDoc:MuPDFDKDoc? = basicDocVc?.session.doc as! MuPDFDKDoc
                myDoc?.deleteSelectedAnnotation()
            }



    .. tab:: Objective-C

        .. code-block:: obj-c

            -(void)deleteDocumentSelection {
                MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
                [myDoc deleteSelectedAnnotation];
            }



Without an annotation being currently selected, calling `deleteSelectedAnnotation()` will have no effect.


Author
--------

To set the author name for subsequent annotation creations, an application developer should set the `documentAuthor` property to the required string value within the `MuPDFDKDoc` instance.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func setAuthor(name:String) {
                let myDoc:MuPDFDKDoc? = basicDocVc?.session.doc as! MuPDFDKDoc
                myDoc?.documentAuthor = name
            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            -(void)setAuthor:(NSString *)name {
                MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
                myDoc.documentAuthor = name;
            }



Targetting document areas
----------------------------

Sometimes in order to focus an annotation, or to suggest an area to annotate, a developer may wish to highlight areas in a document and/or jump to a specific area of interest in a document. There are two API calls available to assist with this.



.. _iOS_Annotations_Outlining:


Outlining an area
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To draw outlines on a document page use `outlineArea`.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.session.doc.outlineArea(<rect>, onPage: <page>)


    .. tab:: Objective-C

        .. code-block:: obj-c

            [basicDocVc.session.doc outlineArea:<rect> onPage:<page>];



Showing an area
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To show an area on a document page use `showArea`.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.showArea(<rect>, onPage: <page>)

    .. tab:: Objective-C

        .. code-block:: obj-c

            [basicDocVc.showArea:<rect> onPage:<page>];





.. include:: footer.rst