.. include:: header.rst



PDF Signatures
==================


.. raw:: html

    <div class="appKitProOnlyMessage"></div><br>


Signatures
----------------

There are two distinct signature types which can be used with App Kit.

- `Digital Signatures`_

- `E-signatures`_

Digital signatures can be placed as signing areas in documents and also signed, whereas E-signatures can just be placed onto a document as part of a signing activity. E-signatures are the simplest use case and just involve a hand drawn signature without digital ID certification.

Digital Signatures
-----------------------

Placing a Digital Signature
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just like regular annotations, the mode within the document view needs to be set.

When the mode is set to "on" then when the user taps on the document a digital signature field will be placed. This field can be moved, repositioned or signed (given that the device has a valid signing certificate installed).

Once the document is saved then the position is set and the digital signature field becomes uneditable.



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.setDigitalSignatureModeOn() // turn on
        documentView.setDigitalSignatureModeOff() // turn off

   .. tab:: Java

      .. code-block:: java

        documentView.setDigitalSignatureModeOn(); // turn on
        documentView.setDigitalSignatureModeOff(); // turn off


Signing a Digital Signature
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Digital signatures use certificate-based digital IDs to authenticate signer identity and demonstrate proof of signing by binding each signature to the document with encryption.

If there is a set digital signature area on your document then tapping that will invoke App Kit to display a pre-built UI for the signing activity. This is a carefully considered UI and will not allow you to continue until you have imported a valid digital ID.


.. image:: images/android/android-choose-cert.jpg
   :align: center



*The UI for choosing a signing certificate*


.. image:: images/android/android-create-signature.jpg
   :align: center


*The UI for digital signing*




E-signatures
-------------------

E-signatures should have been created (i.e. drawn by the user) before they are placed and there is some handy drop-in UI built into App Kit which allows for this. Calling the API to `setESignatureModeOn` (with a single parameter for the view to serve as an anchor for a popup that will appear) will invoke a UI to appear which allows the user to create or place an e-signature accordingly.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.setESignatureModeOn(view) // turn on
        documentView.setESignatureModeOff() // turn off

   .. tab:: Java

      .. code-block:: java

        documentView.setESignatureModeOn(view); // turn on
        documentView.setESignatureModeOff(); // turn off


.. image:: images/android/e-signing.jpg
   :align: center


*The UI for creating your e-signature*


Querying Signature Modes
--------------------------------------

To query for signature mode use the following API:

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val isDigitalSignatureMode:Boolean = documentView.isDigitalSignatureMode
        val isESignatureMode:Boolean = documentView.isESignatureMode

   .. tab:: Java

      .. code-block:: java

        boolean isDigitalSignatureMode = documentView.isDigitalSignatureMode();
        boolean isESignatureMode = documentView.isESignatureMode();


Searching for Signatures
--------------------------------------

If you have a document with multiple digital signatures you can query the document to search through and focus those signatures.


Next Signature
~~~~~~~~~~~~~~~~~~

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.findNextSignature()

   .. tab:: Java

      .. code-block:: java

        documentView.findNextSignature();



Previous Signature
~~~~~~~~~~~~~~~~~~~

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.findPreviousSignature()

   .. tab:: Java

      .. code-block:: java

        documentView.findPreviousSignature();



Signature Count
----------------------

To query a document to find out how many digital signature fields it may contain, use the following API:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val count:Int = documentView.signatureCount

   .. tab:: Java

      .. code-block:: java

        int count = documentView.getSignatureCount();






.. include:: footer.rst