.. include:: header.rst



PDF Table of Contents
=========================




Overview
-----------------

Not all PDF documents will have a Table of Contents, to check to see if the current document instance contains a Table of Contents an application developer should call the `isTOCEnabled` method once the document has fully loaded.



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val hasTOC:Boolean = documentView.isTOCEnabled

        if (hasTOC) {
            // enable UI (e.g. button/gesture) responsible for invoking TOC display
        }

   .. tab:: Java

      .. code-block:: java


        boolean hasTOC = documentView.isTOCEnabled();

        if (hasTOC) {
            // enable UI (e.g. button/gesture) responsible for invoking TOC display
        }


In order to show the pre-built Table of Contents UI an application developer simply needs to call the tableOfContents method as follows:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.tableOfContents()


   .. tab:: Java

      .. code-block:: java


        documentView.tableOfContents();



.. note::

    Table of Contents is also known as "Bookmarks" or "Outline".



Enumerating a Table of Contents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get information about the table of contents the following API should be used to enumerate the listing:



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin


        documentView?.let { dv ->
            // get the TOC entries
            val entries = ArrayList<TocData>()

            dv.enumeratePdfToc(object : DocumentView.EnumeratePdfTocListener {
                override fun nextTocEntry(handle: Int, parentHandle: Int, page: Int,
                                          label: String, url: String, x: Float, y: Float) {
                    val entry: TocData = TocData(handle, parentHandle, page, label, url, x, y)
                    entries.add(entry)
                }
                override fun done() {

                }
            })
        }

        private class TocData constructor(var handle: Int,
                                          var parentHandle: Int,
                                          var page: Int,
                                          var label: String,
                                          var url: String,
                                          var x: Float,
                                          var y: Float) {
            var level:Int = 0
            var tabIndent:String = ""
        }


   .. tab:: Java

      .. code-block:: java

        final ArrayList<TocData> entries = new ArrayList<>();
        documentView.enumeratePdfToc(new DocumentView.EnumeratePdfTocListener() {
            @Override
            public void nextTocEntry(int handle, int parentHandle, int page,
                                     String label, String url, float x, float y) {
                TocData entry = new TocData(handle, parentHandle, page, label, url, x, y);
                entries.add(entry);
            }
            @Override
            public void done() {

            }
        });

        public class TocData {
            public int handle;
            public int parentHandle;
            public String label;
            private String url;
            private int page;
            private float x;
            private float y;

            TocData(int handle, int parentHandle, int page, String label, String url, float x, float y) {
                this.handle = handle;
                this.parentHandle = parentHandle;
                this.page = page;
                this.label = label;
                this.url = url;
                this.x = x;
                this.y = y;
            }
        }






.. include:: footer.rst