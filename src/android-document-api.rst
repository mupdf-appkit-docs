.. include:: header.rst



Document API
========================



Options
----------

An application developer can register options for the following in the App Kit SDK:


.. raw:: html

    <table>
    <thead>
    <tr>
    <th>Property</th>
    <th>Capability</th>
    </tr>
    </thead>
    <tbody><tr>
    <td>Editing</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Save As</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Open In</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Sharing</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>External Clipboard In</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>External Clipboard Out</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Printing</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Launch External Url</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Form Filling <span class="appKitProFeatureMessage"></span></td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Form Signing <span class="appKitProFeatureMessage"></span></td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Redactions <span class="appKitProFeatureMessage"></span></td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Full Screen</td>
    <td><em>Enable</em></td>
    </tr>
    <tr>
    <td>Invert Content In Dark Mode</td>
    <td><em>Enable</em></td>
    </tr>
    </tbody></table>




To do so a developer should instantiate `ConfigOptions`, set the required variables within that object, and then register it against the `SODKLib` object.

The following code example disables editing on a document:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.solib.ConfigOptions
        import com.artifex.solib.ArDkLib

        fun setupConfigOptions() {
            var configOptions:ConfigOptions = ConfigOptions()
            configOptions.isEditingEnabled = false
            ArDkLib.setAppConfigOptions(configOptions)
        }

   .. tab:: Java

      .. code-block:: java

        import com.artifex.solib.ConfigOptions;
        import com.artifex.solib.SODKLib;

        public void setupConfigOptions() {
            ConfigOptions configOptions = new ConfigOptions();
            configOptions.setEditingEnabled(false);
            ArDkLib.setAppConfigOptions(configOptions);
        }


.. _Android_DocumentAPI_Listeners:

Listeners
--------------

Document listeners should only be required when using the :ref:`Custom UI<Android_CustomUI>` as the application developer is responsible for providing their own UI to manage relevant document events.

Available document listeners are as follows:

Page Loaded
~~~~~~~~~~~~~~~

Called when pages are loaded.

Document Completed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called when the document has completely loaded.

Password Required
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called when a password is required by the document.

On View Changed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called when the scale, scroll, or selection in the document changes.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.setDocumentListener(object : DocumentListener {
            override fun onPageLoaded(p0: Int) {

            }

            override fun onDocCompleted() {

            }

            override fun onPasswordRequired() {

            }

            override fun onViewChanged(scale: Float,
             scrollX: Int,
             scrollY: Int,
             selectionRect: Rect?) {

            }
        }

   .. tab:: Java

      .. code-block:: java


        documentView.setDocumentListener(new DocumentListener() {
            @Override
            public void onPageLoaded(int pagesLoaded) {

            }

            @Override
            public void onDocCompleted() {

            }

            @Override
            public void onPasswordRequired() {

            }

            @Override


            public void onViewChanged(float scale,
            int scrollX,
            int scrollY,
            Rect selectionRect) {

            }
        });




On Update UI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called when UI updates are invoked by DocumentView.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.setOnUpdateUI { }


   .. tab:: Java

      .. code-block:: java


        documentView.setOnUpdateUI(new Runnable() {
            @Override
            public void run() {

            }
        });




Page Change
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called on a page change event - i.e. when the document has scrolled or jumped to another page.



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.setPageChangeListener { pageNumber ->

        }

   .. tab:: Java

      .. code-block:: java

        documentView.setPageChangeListener(new DocumentView.ChangePageListener() {
            @Override
            public void onPage(int pageNumber) {

            }
        });








Full Screen Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A `DocumentView` document has the ability to fill the screen and enter a uneditable mode for an optimal reading experience. It is the application developer's responsibility to turn off the UI that they do not wish to see when this mode is invoked and to ensure that their `DocumentView` instance fills the device screen. To turn desired UI back on again the `DocumentView` instance will invoke the application developer's closure method upon exiting full screen mode ( when the user taps the screen ).




.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        findViewById<View>(R.id.fullScreenButton).setOnClickListener {
            // hide this activity's UI

            // put DocumentView in full screen mode
            if (dv != null) {
                dv.enterFullScreen {
                    // closure method to restore our UI upon exit
                }
            }
        }



   .. tab:: Java

      .. code-block:: java

        findViewById(R.id.fullScreenButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hide this activity's UI

                // put DocumentView in full screen mode
                if (documentView != null) {
                    documentView.enterFullScreen(new Runnable() {
                        @Override
                        public void run() {
                            // closure method to restore our UI upon exit
                        }
                    });
                }
            }
        });



Document Actions
---------------------

Providing a Password
~~~~~~~~~~~~~~~~~~~~~~~~~

An application developer should provide a way to enter a password for a document and then provide it to the document view as follows:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.providePassword("my-password")

   .. tab:: Java

      .. code-block:: java

        documentView.providePassword("my-password");







Undo
~~~~~~~~~~~~~~~~~~~~~~~~~

To undo a previous action (such as adding an annotation) use the the following method against your document instance.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        if (documentView.canUndo()) {
            documentView.undo()
        }

   .. tab:: Java

      .. code-block:: java

        if (documentView.canUndo()) {
            documentView.undo();
        }




Redo
~~~~~~~~~~~~~~~~~~~~~~~~~

To redo a previous action (such as adding an annotation) use the the following method against your document instance.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        if (documentView.canRedo()) {
            documentView.redo()
        }

   .. tab:: Java

      .. code-block:: java

        if (documentView.canRedo()) {
            documentView.redo();
        }




Get Selected Text
~~~~~~~~~~~~~~~~~~~~~~~

To get the selected text from a document an application developer should request the `selectedText` property against the `DocumentView` instance.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val selectedText:String? = documentView.selectedText

   .. tab:: Java

      .. code-block:: java

        String selectedText = documentView.getSelectedText();



.. note::

    If there is no selected text in the document then a `null` value will be returned.


Can Select
~~~~~~~~~~~~~~~~~~~~~~~

To query whether you are able to select objects in a document ( i.e. if the document is in read-only mode or not ) then use the following:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val canSelect:Boolean = documentView.canSelect()

   .. tab:: Java

      .. code-block:: java

        Boolean canSelect = documentView.canSelect();



.. include:: footer.rst