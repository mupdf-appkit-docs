.. include:: header.rst



PDF Annotations
==================


Annotations
-----------------

Annotations include allowing :ref:`drawing<Android_Annotations_Draw_Mode>`, :ref:`adding notes<Android_Annotations_Note_Mode>` and :ref:`highlighting<Android_Annotations_Highlighting>` text on a PDF document.



.. _Android_Annotations_Draw_Mode:

Draw Mode
---------------

Turning on drawing is as simple as calling the `setDrawModeOn()` method against your `DocumentView` instance. To turn off drawing just call the `setDrawModeOff()` method against your `DocumentView` instance.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

        fun setDocumentDrawMode(documentView:DocumentView, enable:Boolean) {
            if (enable) {
                documentView.setDrawModeOn()
            } else {
                documentView.setDrawModeOff()
            }
        }


   .. tab:: Java

      .. code-block:: java


        import com.artifex.sonui.editor.DocumentView;

        public void setDocumentDrawMode(documentView:DocumentView, enable:Boolean) {
            if (enable) {
                documentView.setDrawModeOn();
            } else {
                documentView.setDrawModeOff();
            }
        }


When Draw Mode is enabled, the user can draw an ink annotation with the selected line thickness and color. When Draw Mode is then disabled, the annotation is saved to the document.

Line Thickness
~~~~~~~~~~~~~~~~~~

To get/set the line thickness use the API as follows:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val thickness:Float = documentView.lineThickness // getter
        documentView.lineThickness = 2.0f // setter

   .. tab:: Java

      .. code-block:: java

        float thickness = documentView.getLineThickness(); // getter
        documentView.setLineThickness(2.0f); // setter





Line Color
~~~~~~~~~~~~~~~~~~

To get/set the line color use the API as follows:

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val color:Int = documentView.lineColor // getter
        documentView.lineColor = 0xFF00FF00 // setter

   .. tab:: Java

      .. code-block:: java

        int color = documentView.getLineColor(); // getter
        documentView.setLineColor(0xFF00FF00); // setter


.. _Android_Annotations_Note_Mode:

Note Mode
------------------

Depending on the mode the user will be able to add notes onto the document or not.

To turn on note mode call the `setNoteModeOn()` method against your `DocumentView` instance. To turn off note mode just call the corresponding `setNoteModeOff()` method against your `DocumentView` instance.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

        fun setDocumentNoteMode(documentView:DocumentView, enable:Boolean) {
            if (enable) {
                documentView.setNoteModeOn()
            } else {
                documentView.setNoteModeOff()
            }
        }


   .. tab:: Java

      .. code-block:: java


        import com.artifex.sonui.editor.DocumentView;

        public void setDocumentNoteMode(documentView:DocumentView, enable:Boolean) {
            if (enable) {
                documentView.setNoteModeOn();
            } else {
                documentView.setNoteModeOff();
            }
        }




.. _Android_Annotations_Highlighting:


Highlighting
-------------------

In order to create a highlight annotation on some text in a PDF document, you must first have some text selected. Once selected call the `highlightSelection()` method against your `DocumentView` instance.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

        fun setDocumentHighlight(documentView:DocumentView)  {
            documentView.highlightSelection()
        }


   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.DocumentView;

        public void setDocumentHighlight(documentView:DocumentView) {
            documentView.highlightSelection();
        }



.. note::

    Without a text selection being present in your document view calling `highlightSelection()` will have no effect.



Deleting Annotations
------------------------------

Essentially all annotations are treated the same as simply selections once they have been selected by a user. This selection can then be removed by calling the `deleteSelection()` method against the `DocumentView` instance.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

        fun deleteDocumentSelection(documentView:DocumentView)  {
            documentView.deleteSelection()
        }


   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.DocumentView;

        public void deleteDocumentSelection(documentView:DocumentView) {
            documentView.deleteSelection();
        }



.. note::

    Without an annotation being currently selected in your document view calling `deleteSelection()` will have no effect.



Author
----------

To get/set the annotation author use the API as follows:

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val author:String = documentView.author // getter
        documentView?.author = "John Doe" // setter

   .. tab:: Java

      .. code-block:: java

        String author = documentView.getAuthor();
        documentView.setAuthor("Jane Doe");




.. include:: footer.rst