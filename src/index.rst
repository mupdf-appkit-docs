Welcome to the :title:`MuPDF App Kit` documentation
======================================================


:title:`MuPDF App Kit` libraries enable quick and easy :title:`PDF` document viewing and editing for :title:`Android` and :title:`iOS` platforms.



----


.. toctree::
   :caption: Getting Started
   :maxdepth: 3

   guide.rst
   about.rst
   download.rst




.. toctree::
   :caption: API Reference
   :maxdepth: 3

   api.rst



