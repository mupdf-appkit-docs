.. include:: header.rst




PDF Redactions
==================


.. raw:: html

    <div class="appKitProOnlyMessage"></div><br>


Redactions
-------------------

The redaction feature has been designed to work on text, images, and links. By selecting all or part of an image, text, or link, and applying redaction this then permanently redacts the selected information, making it impossible to retrieve the original data.

Redacted text and areas can be :ref:`marked<iOS_Redactions_Mark>`, and any marked redactions can be :ref:`removed<iOS_Redactions_Remove>`, however once the redactions are :ref:`applied<iOS_Redactions_Apply>` and the document is saved then the redacted information is blocked out with black to denote the area of redaction. At this point the original text and/or area is unretrievable.





Redaction Modes
---------------------

Your custom UI can support redaction by employing three specific annotating modes.


.. _iOS_Redactions_Mark:

Edit Redaction Mode
~~~~~~~~~~~~~~~~~~~~~~~~

Firstly set the annotation mode for your document view to `MuPDFDKAnnotatingMode_EditRedaction`. This ensures that only redaction markings can be selected. When selected they can be adjusted or removed.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_EditRedaction


    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_EditRedaction;



After adding a redaction it is advised to return your `MuPDFDKBasicDocumentViewController` back to this edit mode as default.




Text Redaction Mode
~~~~~~~~~~~~~~~~~~~~~~~~

Set the annotation mode for your document to `MuPDFDKAnnotatingMode_RedactionTextSelect`. This prepares the document view to mark text for redaction.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionTextSelect

    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionTextSelect;



In this mode, the user can create a redaction marking by dragging across text, whereupon the mode will revert to `MuPDFDKAnnotatingMode_EditRedaction`, leaving the newly created marking of the text selected. The selection will show drag handles via which the user can adjust it.


Area Redaction Mode
~~~~~~~~~~~~~~~~~~~~~~~~

Set the annotation mode for your document to `MuPDFDKAnnotatingMode_RedactionAreaSelect`. This prepares the document view to mark an area for redaction.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionAreaSelect

    .. tab:: Objective-C

        .. code-block:: obj-c

            basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionAreaSelect;


In this mode, the user can create a redaction marking by dragging out an area from one corner to the diagonally opposite one, whereupon the mode will revert to `MuPDFDKAnnotatingMode_EditRedaction`, leaving the newly created marking of an area selected. The selection will show drag handles via which the user can adjust it.



Marking Already Selected Text
--------------------------------


There is an alternative way to mark text for redaction. If the user has already selected an area of text, it is possible to mark that text for redaction by calling the `addRedactAnnotation()` method against your `MuPDFDKDoc` instance. The document's currently selected text will then be marked with a red box outline to denote the redaction demarcation.



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
            myDoc.addRedactAnnotation()


    .. tab:: Objective-C

        .. code-block:: obj-c

            MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
            [myDoc addRedactAnnotation];


De-selecting a Redaction
--------------------------------

A selected redaction can then be cleared (de-selected) by calling the `clearSelection()` method against the `MuPDFDKDoc` instance within `MuPDFDKBasicDocumentViewController`.

.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
            myDoc.clearSelection()



    .. tab:: Objective-C

        .. code-block:: obj-c

            MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
            [myDoc clearSelection];


.. _iOS_Redactions_Remove:

Removing a Marked Redaction
----------------------------------------------------------------

To remove a marked redaction call the `deleteSelectedAnnotation()` method against your `MuPDFDKDoc` instance. The document's currently selected marked for redaction ( i.e. a selection of text with the red box outline ) will then no longer be marked for redaction.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
            myDoc.deleteSelectedAnnotation()


    .. tab:: Objective-C

        .. code-block:: obj-c

            MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
            [myDoc deleteSelectedAnnotation];


.. _iOS_Redactions_Apply:

Applying Redactions
----------------------------------------------------------------

Applying redactions means that all document selections which are marked for redaction will be redacted. To apply redactions call the `finalizeRedactAnnotations()` method against your `MuPDFDKDoc`z instance. Once redactions have been applied then the redacted text is blocked out with black.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
            myDoc.finalizeRedactAnnotations({
                /// onComplete
            })

    .. tab:: Objective-C

        .. code-block:: obj-c

            MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
            [myDoc finalizeRedactAnnotations:^{
                /// onComplete
            }];


.. note::

    Redactions are not permanently set until the document is saved. If the document is exited without saving then the redactions will be lost.



.. include:: footer.rst