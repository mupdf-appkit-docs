.. include:: header.rst



.. _App_Kit_Download:

Download App Kit
======================



:title:`Artifex` supplies projects, available to try for free, which utilize :title:`App Kit` for both :title:`Android` and :title:`iOS`. Please use :title:`Android Studio` or :title:`Apple's Xcode` to try the projects out.

The sample projects contain the :title:`App Kit` libraries, which are built using the corresponding version of :title:`MuPDF`.

The demo version of :title:`MuPDF App Kit` contains a watermark. To remove the watermark, you will need to purchase a commercial license from :title:`Artifex`. Instructions on using the key to remove the watermark can be found in the "Getting Started" sections of this documentation.



.. raw:: html

    <button class="cta orange" onclick="window.open('https://artifex.com/mupdf-appkit/', '_blank')">TRY FOR FREE</button>



.. include:: footer.rst