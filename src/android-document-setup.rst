.. include:: header.rst



Document Setup
==================




Setup
----------

PDF documents in App Kit are always rendered inside a document view instance. Files which require to instantiate and access document views should reference the following:



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.DocumentView;



Your Android `Activity` should handle the regular Android Activity Lifecycle events and inform any `DocumentView` instance of the corresponding `Activity Events`_. Additionally `Activity Interfaces`_ require to be setup for full App Kit functionality.

For the :ref:`Custom UI<Android_CustomUI>`, there are also a set of common document events during the lifecycle of a document. An application developer can set up :ref:`Listeners<Android_DocumentAPI_Listeners>` to respond to these events as required.


Activity Events
--------------------

Other events should be passed through to your `DocumentView` instance as follows:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        public override fun onPause() {
            super.onPause()
            documentView?.onPause()
        }

        override fun onResume() {
            super.onResume()
            documentView?.onResume()
        }

        override fun onDestroy() {
            super.onDestroy()
            documentView?.onDestroy()
        }

        override fun onBackPressed() {
            documentView?.onBackPressed()
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            documentView?.onActivityResult(requestCode, resultCode, data)
        }

        override fun onConfigurationChanged(newConfig: Configuration) {
            super.onConfigurationChanged(newConfig)
            documentView?.onConfigurationChange(newConfig)
        }

   .. tab:: Java

      .. code-block:: java

        @Override
        public void onPause() {
            if (documentView != null)
                documentView.onPause();
            super.onPause();
        }

        @Override
        protected void onResume() {
            super.onResume();
            if (documentView != null)
                documentView.onResume();
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            if (documentView != null)
                documentView.onDestroy();
        }

        @Override
        public void onBackPressed() {
            if (documentView != null)
                documentView.onBackPressed();
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (documentView != null)
                documentView.onActivityResult(requestCode, resultCode, data);
        }

        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);

            if (documentView != null)
                documentView.onConfigurationChange(newConfig);
        }


.. note::

        The above events with their corresponding document view calls are critical for document text editing and selection.


Activity Interfaces
----------------------

Security setup
~~~~~~~~~~~~~~~~~


There are four optional interfaces that can be implemented via your own custom classes in order to define how App Kit manages data security.




- SODataLeakHandlers_
- SOPersistentStorage_
- SOClipboardHandler_
- SOSecureFS_


.. note::

    There are no defaults for these interfaces.


These interfaces are specifically important when considering a :ref:`Custom UI<Android_CustomUI>` approach.

Taken together, your own class implementations following these interfaces can be used to implement security at all the points where a user’s data might flow into, or out of, the application.

SODataLeakHandlers
""""""""""""""""""""""""

An interface that specifies the basis for implementing a class to provide hooks for an app to control file saving and other functions.

SOPersistentStorage
""""""""""""""""""""""""

An interface that specifies the basis for implementing a class allowing for storage/retrieval of key/value pairs.

SOClipboardHandler
""""""""""""""""""""""""

An interface that specifies the basis for implementing a class to handle clipboard actions for the document editor.

SOSecureFS
""""""""""""""""""""""""

An interface that specifies the basis for implementing a class to allow proprietary encrypted files, stored in a secure container. A developer can use this opportunity to enforce security and role restrictions, or map the file operations onto another mechanism, such as a database.

If required, the following code ( with it's own implementations of these classes ) should be invoked at the start of your app's main activity as part of your setup.




.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.solib.*
        import com.artifex.sonui.editor.Utilities

        public override fun onCreate(savedInstanceState: Bundle) {
            super.onCreate(savedInstanceState)

            Utilities.setDataLeakHandlers(MyOwnDataLeakHandlers())
            Utilities.setPersistentStorage(MyOwnPersistentStorage())
            ArDkLib.setClipboardHandler(MyOwnClipboardHandler())
            ArDkLib.setSecureFS(MyOwnSecureFS())

            ...
        }



   .. tab:: Java

      .. code-block:: java

        import com.artifex.solib.*;
        import com.artifex.sonui.editor.Utilities;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Utilities.setDataLeakHandlers(new MyOwnDataLeakHandlers());
            Utilities.setPersistentStorage(new MyOwnPersistentStorage());
            ArDkLib.setClipboardHandler(new MyOwnClipboardHandler());
            ArDkLib.setSecureFS(new MyOwnSecureFS());

            ...
        }





.. include:: footer.rst