.. include:: header.rst



Getting Started
==================



System requirements
---------------------------

Android minimum SDK: The MuPDF library needs Android version 6.0 or newer. Make sure that the `minSdkVersion` in your app's `build.gradle` is at least 23.



.. code-block::

    android {
        defaultConfig {
            minSdkVersion 23
            ...
        }
        ...
    }



Adding the App Kit to your project
------------------------------------------------------


In order to include MuPDF in your app you need to use the Gradle build system.

The MuPDF App Kit can be retrieved as pre-built artifacts from a local `app/libs` folder location relative to your Android project. Your Android project's module `build.gradle` should add this location.

To add the MuPDF App Kit to your project add the dependencies section in your `Module` `build.gradle`:



.. code-block::

    dependencies {
        ...
        // Dependency on view binding
        implementation 'androidx.databinding:viewbinding:7.1.2'
        // Dependency on navigation fragments
        implementation 'androidx.navigation:navigation-fragment-ktx:2.2.2'
        implementation 'androidx.navigation:navigation-ui-ktx:2.2.2'
        // Dependency on local binaries
        implementation fileTree(dir: 'libs', include: ['*.aar'])
    }


Fetching the artifacts
~~~~~~~~~~~~~~~~~~~~~~~~~~


Ensure to follow these steps:

- :ref:`Download<App_Kit_Download>` the latest MuPDF App Kit.

- Then copy all the aar files contained in `mupdf-test/app/libs` to your own app's `app/libs/` folder.

- Sync your `build.gradle` and then the App Kit libraries should be correctly configured and ready for use.


.. _Android_License_Key:

License Key
------------------

To remove the MuPDF document watermark from your App, you will need to use a license key to activate App Kit.

To acquire a license key for your app you should:

#. Go to `artifex.com/appkit`_
#. Purchase your license(s)
#. In your application code add the API call to activate the license

.. note::

    App Kit does not require network connection to validate a license key and there is no server tracking or cloud logging involved with key validation.


Using your License Key
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have a license key for MuPDF App Kit, it will be bound to the App ID which you will have defined at the time of purchase. Therefore you should ensure that the App ID in your Android project is correctly set. This is defined in configuration within the Gradle file for the app (`build.gradle(:app)`).


.. code-block:: groovy

    defaultConfig {
          applicationId "your.license.key.app.id"
          ...
    }


Once you have confirmed that the `applicationId` is correctly named, then call the following API early on in your application code:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView
        ...

        val ctx: Context = this
        val licenseKey:String = "put your license key here"
        val ok:Boolean = DocumentView.unlockAppKit(ctx, licenseKey)


   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.DocumentView;
        ...

        Context ctx = this;
        String licenseKey = "put your license key here";
        Boolean ok = DocumentView.unlockAppKit(ctx, licenseKey);


.. note::

    The `Context` parameter should reference your main application `Activit`y.

    The API call to set the license key should be made before you instantiate an App Kit `DocumentView`.


.. _artifex.com/appkit : https://artifex.com/mupdf-appkit



.. include:: footer.rst