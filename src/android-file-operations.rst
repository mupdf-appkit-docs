.. include:: header.rst



File Operations
==================


There are a number of file operations available against a document, an application developer should only be required to implement these if using the :ref:`Custom UI<Android_CustomUI>`.

Save
------

Saving a document only needs to be invoked if there are changes made to a document. As such an application developer can verify if changes have been made or not and act accordingly.



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        if (documentView.isDocumentModified {
            documentView.save()
        }


   .. tab:: Java

      .. code-block:: java


        if (documentView.isDocumentModified()) {
            documentView.save();
        }


Save As
-------------

When saving a document using this method an application developer must provide a valid path on the file system to save to.

.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val docPath:String = "<YOUR_DOCUMENT_PATH>"

        documentView.saveTo(docPath) { result, err ->
            if (result == SODocSaveListener.SODocSave_Succeeded) {
                // success
            } else {
                // error
            }
        }



   .. tab:: Java

      .. code-block:: java

        String docPath = "<YOUR_DOCUMENT_PATH>";

        documentView.saveTo(newPath, new SODocSaveListener() {
            @Override
            public void onComplete(int result, int err) {
                if (result == SODocSave_Succeeded) {
                    // success
                } else {
                    // error
                }
            }
        });




Export
-----------

It is possible to export the content of a PDF into an external text file for simple text extraction. To do so an application developer should define a valid file path to use with the `exportToAPI`.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.exportTo("filePath", "txt") { result, err ->
            if (result == SODocSaveListener.SODocSave_Succeeded) {
                // success
            } else {
                // error
            }
        }


   .. tab:: Java

      .. code-block:: java


        documentView.exportTo("filePath", "txt", (result, err) -> {
            if (result == SODocSaveListener.SODocSave_Succeeded) {
                // success
            } else {
                // error
            }
        });



.. note::

    At present the only valid format to export to is a text file, so the format parameter should always be set to "txt".

    Further formats will become available later.


Print
--------

Application developers should call the `print()` method against the `DocumentView` instance to open up the print dialog.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.print()

   .. tab:: Java

      .. code-block:: java

        documentView.print();



Search
----------

Searching is invoked from the current document selection or cursor position and can be made forward or backward from this point. Successful searching automatically highlights the next instance of a found String and moves the document selection to that point.


.. note::

    Search is case-insensitive.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        fun search(text:String, forward:Boolean) {
            if (forward) {
                documentView.searchForward(text)
            } else {
                documentView.searchBackward(text)
            }
        }


   .. tab:: Java

      .. code-block:: java

        private void search(String text, Boolean forward) {
            if (forward) {
                documentView.searchForward(text);
            } else {
                documentView.searchBackward(text);
            }
        }







.. include:: footer.rst