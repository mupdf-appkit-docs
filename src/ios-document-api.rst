.. include:: header.rst


.. _iOS_Document_API:

Document API
==================



Delegates
-----------------

When developing your own custom UI, your application's `ViewController` should, at a minimum, implement the `ARDKBasicDocViewDelegate` and `ARDKDocumentEventTarget` protocols to intercept events.


.. _iOS_ARDKBasicDocViewDelegate:

ARDKBasicDocViewDelegate
----------------------------


.. _iOS_DocumentAPI_Document_Completed:

Document Completed
~~~~~~~~~~~~~~~~~~~~~~~~~

Called when the document has completely loaded.



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Inform the UI that both the loading of the document
            /// and the initial rendering have completed. An app might
            /// display a busy indicator while a document is initially
            /// loading, and use this delegate method to dismiss the
            /// indicator.
            func loadingAndFirstRenderComplete() {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Inform the UI that both the loading of the document
            /// and the initial rendering have completed. An app might
            /// display a busy indicator while a document is initially
            /// loading, and use this delegate method to dismiss the
            /// indicator.
            - (void)loadingAndFirstRenderComplete {

            }


UI Update
~~~~~~~~~~~~~~~~~~~~~~~~~

Called when the document changes selection state and the UI should update appropriately.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Tell the UI to update according to changes in the
            /// selection state of the document. This allows an app
            /// to refresh any currently displayed state information.
            /// E.g., a button used to toggle whether the currently
            /// selected text is bold, may show a highlight to indicate
            /// bold or not. This call would be the appropriate place to
            /// ensure that highlight reflects the current state.
            func updateUI() {

            }

    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Tell the UI to update according to changes in the
            /// selection state of the document. This allows an app
            /// to refresh any currently displayed state information.
            /// E.g., a button used to toggle whether the currently
            /// selected text is bold, may show a highlight to indicate
            /// bold or not. This call would be the appropriate place to
            /// ensure that highlight reflects the current state.
            - (void)updateUI {

            }

Detecting Page Movement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following method is called when the document is moved - i.e. on pan or zoom events.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Tell the UI that the view of the document has moved. This
            /// may be called very frequently if the document is scrolling,
            /// and can be used to keep UI elements positioned next to items
            /// within the document, such as the current selection.
            func viewDidMove() {

            }

    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Tell the UI that the view of the document has moved. This
            /// may be called very frequently if the document is scrolling,
            /// and can be used to keep UI elements positioned next to items
            /// within the document, such as the current selection.
            - (void)viewDidMove {

            }

Page Change
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called on a page change event - i.e. when the document has scrolled or jumped to another page.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Tell the UI that the document has scrolled to a new page.
            /// An app may use this to update a label showing the current
            /// displayed page number or to scroll a view of thumbnails
            /// to the correct page.
            func viewDidScroll(toPage page: Int) {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Tell the UI that the document has scrolled to a new page.
            /// An app may use this to update a label showing the current
            /// displayed page number or to scroll a view of thumbnails
            /// to the correct page.
            - (void)viewDidScrollToPage:(NSInteger)page {

            }


Scrolling Completed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called when a user scrolling event has concluded it's animation.

.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Tell the delegate when a scrolling animation concludes.
            /// This can be used like viewDidScrollToPage, but for more
            /// intensive tasks that one wouldn't want to run repeatedly
            /// during scrolling.
            func scrollViewDidEndScrollingAnimation() {

            }

    .. tab:: Objective-C

        .. code-block:: obj-c


            /// Tell the delegate when a scrolling animation concludes.
            /// This can be used like viewDidScrollToPage, but for more
            /// intensive tasks that one wouldn't want to run repeatedly
            /// during scrolling.
            - (void)scrollViewDidEndScrollingAnimation {

            }


.. _iOS_DocumentAPI_Swallowing_Tap_Selections:


Swallowing Tap Selections
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An application developer has the ability to intercept tap events on the document to prevent a default selection from occurring. To do this the following delegate methods need to return false. Essentially a document is in a read only state in this mode.



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Offer the UI the opportunity to swallow a tap that
            /// may have otherwise caused selection. Return YES
            /// to swallow the event. This is not called for taps
            /// over links or form fields. An app might use this to
            /// provide a way out of a special mode (full-screen for
            /// example). In that case, if the app is using the tap to
            /// provoke exit from full-screen mode, then it would return
            /// YES from this method to avoid the tap being interpreted
            /// also by the main document view.
            func swallowSelectionTap() -> Bool {
                return false
            }

            /// Offer the UI the opportunity to swallow a double tap that
            /// may have otherwise caused selection. Return YES to swallow
            /// the event. This is not called for double taps over links
            /// or form fields. An app might use this in a way similar to
            /// that appropriate to swallowSelectionTap.
            func swallowSelectionDoubleTap() -> Bool {
                return false
            }

    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Offer the UI the opportunity to swallow a tap that
            /// may have otherwise caused selection. Return YES
            /// to swallow the event. This is not called for taps
            /// over links or form fields. An app might use this to
            /// provide a way out of a special mode (full-screen for
            /// example). In that case, if the app is using the tap to
            /// provoke exit from full-screen mode, then it would return
            /// YES from this method to avoid the tap being interpreted
            /// also by the main document view.
            - (BOOL)swallowSelectionTap {
                return NO;
            }

            /// Offer the UI the opportunity to swallow a double tap that
            /// may have otherwise caused selection. Return YES to swallow
            /// the event. This is not called for double taps over links
            /// or form fields. An app might use this in a way similar to
            /// that appropriate to swallowSelectionTap.
            - (BOOL)swallowSelectionDoubleTap {
                return NO;
            }


.. note::

    It is important that these delegate methods return `true` by default for expected text and annotation selection behaviour to occur.


Inhibiting the Keyboard
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If required an application developer can prevent the keyboard from appearing.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// Called to allow the delegate to inhibit the keyboard. An app
            /// might use this in special modes where there is limited
            /// vertical space, so as to avoid the keyboard appearing.
            func inhibitKeyBoard() -> Bool {
                return false
            }

    .. tab:: Objective-C

        .. code-block:: obj-c

            /// Called to allow the delegate to inhibit the keyboard. An app
            /// might use this in special modes where there is limited
            /// vertical space, so as to avoid the keyboard appearing.
            - (BOOL)inhibitKeyBoard {
                return NO;
            }



Opening a URL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This method is called when the document interaction invokes a URL to open.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            /// The document view calls this when
            /// a link to an external document is tapped.
            func callOpenUrlHandler(_ url: URL!, fromVC presentingView: UIViewController!) {

            }

    .. tab:: Objective-C

        .. code-block:: obj-c

            /// The document view calls this when
            /// a link to an external document is tapped.
            - (void)callOpenUrlHandler:(NSURL *)url fromVC:(UIViewController *)presentingView {

            }


.. _iOS_ARDKDocumentEventTarget:

ARDKDocumentEventTarget
----------------------------

Page Load Events and Loading Complete
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Called as pages are loaded from the document.


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            /// Called as pages are loaded from the document.
            /// There may be further calls, e.g., if pages
            /// are added or deleted from the document.
            func updatePageCount(_ pageCount: Int, andLoadingComplete complete: Bool) {

            }


    .. tab:: Objective-C

       .. code-block:: obj-c

            /// Called as pages are loaded from the document.
            /// There may be further calls, e.g., if pages
            /// are added or deleted from the document.
            - (void)updatePageCount:(NSInteger)pageCount andLoadingComplete:(BOOL)complete {

            }


Called when layout has completed.


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            func layoutHasCompleted() {

            }


    .. tab:: Objective-C

       .. code-block:: obj-c

            - (void)layoutHasCompleted {

            }


Called when the page size changes.


.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            func pageSizeHasChanged() {

            }


    .. tab:: Objective-C

       .. code-block:: obj-c

            - (void)pageSizeHasChanged {

            }


Selection Changes
~~~~~~~~~~~~~~~~~~~~~~~~~

Called when a selection is made within the document, moved or removed.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func selectionHasChanged() {

            }

            // there is also a function method which can be
            // associated against the instance of `MuPDFDKDoc` as follows
            doc.onSelectionChanged = {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            - (void)selectionHasChanged {

            }

            // there is also a function method which can be
            // associated against the instance of `MuPDFDKDoc` as follows
            doc.onSelectionChanged = ^() {

            };



Selection Types
~~~~~~~~~~~~~~~~~~~~~~~~~

Once a selection has been made on a document it might be necessary to understand what type of selection it is and if there is any further data. For example, is the user's selection a redaction annotation or is it a note annotation? Does the selected annotation have a date associated with it?

To determine this information, the following type of query against the document instance (`MuPDFDKDoc`) can be made:

.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            let selectionIsRedaction:Bool = doc.selectionIsRedaction
            let selectionIsNote:Bool = doc.selectionIsAnnotationWithText
            let selectedAnnotationsDate:Date? = doc.selectedAnnotationsDate
            let haveTextSelection:Bool = doc.haveTextSelection


    .. tab:: Objective-C

        .. code-block:: obj-c

            BOOL selectionIsRedaction = doc.selectionIsRedaction;
            BOOL selectionIsNote = doc.selectionIsAnnotationWithText;
            NSDate* selectedAnnotationsDate = doc.selectedAnnotationsDate;
            BOOL haveTextSelection = doc.haveTextSelection;


The following table defines the full set of available selections:

.. raw:: html

    <table>
    <thead>
    <tr>
    <th>Variable name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Return type</th>
    </tr>
    </thead>
    <tbody><tr>
    <td>selectionIsWidget</td>
    <td>form widget</td>
    <td><em>Whether a form widget is currently selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>selectedAnnotationsText</td>
    <td>text</td>
    <td><em>The text string associated with the selected annotation</em></td>
    <td><code>String</code> or <code>nil</code></td>
    </tr>
    <tr>
    <td>selectedAnnotationsDate</td>
    <td>date</td>
    <td><em>The date associated with the selected annotation</em></td>
    <td><code>Date</code> or <code>nil</code></td>
    </tr>
    <tr>
    <td>selectedAnnotationsAuthor</td>
    <td>author</td>
    <td><em>The author of the selected annotation</em></td>
    <td><code>String</code> or <code>nil</code></td>
    </tr>
    <tr>
    <td>selectionIsRedaction</td>
    <td>redaction</td>
    <td><em>Whether a redaction annotation is selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>selectionIsStamp</td>
    <td>stamp</td>
    <td><em>Whether a stamp annotation is selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>selectionIsTextHighlight</td>
    <td>highlight</td>
    <td><em>Whether a text highlight annotation is selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>selectionIsAnnotationWithText</td>
    <td>note</td>
    <td><em>Whether an annotation that has text is selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>haveTextSelection</td>
    <td>text</td>
    <td><em>Whether text is currently selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    <tr>
    <td>haveAnnotationSelection</td>
    <td>any</td>
    <td><em>Whether an annotation is currently selected</em></td>
    <td><code>Bool</code></td>
    </tr>
    </tbody></table>


.. _iOS_ARDKPageSelectorDelegate:

ARDKPageSelectorDelegate
---------------------------

Select Page
~~~~~~~~~~~~~~~~

Called when the document page selector selects a page.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func selectPage(_ page: Int) {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            -(void) selectPage:(NSInteger)page {

            }


Delete Page
~~~~~~~~~~~~~~~~

Called when the document page selector deletes a page.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func deletePage(_ page: Int) {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            -(void) deletePage:(NSInteger)page {

            }

Duplicate Page
~~~~~~~~~~~~~~~~

Called when the document page selector duplicates a page.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func duplicatePage(_ page: Int) {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            -(void) duplicatePage:(NSInteger)page {

            }

Move Page
~~~~~~~~~~~~~~~~

Called when the document page selector moves a page.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            func movePage(_ page: Int, to newPos: Int) {

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            - (void)movePage:(NSInteger)pageNumber to:(NSInteger)newNumber {

            }


.. _iOS_DocumentAPI_Listeners:


Listeners
---------------

Document Load
~~~~~~~~~~~~~~~~~~

An application developer can listen for basic success or error for a document load.

When a document load is requested, the following function blocks can be defined for the document (i.e. the instance of `MuPDFDKDoc`).



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            doc.successBlock = {

            }

            doc.errorBlock = {(error:ARDKDocErrorType?) in

            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            doc.successBlock = ^() {

            };

            doc.errorBlock = ^(ARDKDocErrorType error) {

            };



Request Password
~~~~~~~~~~~~~~~~~~~~~

For documents which may be password protected a developer should set a method against the document session to be triggered on the event of a password protected file attempting to load.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            session.passwordRequestBlock = {[weak self] in

            }



    .. tab:: Objective-C

        .. code-block:: obj-c

            session.passwordRequestBlock = ^{

            };


Typically the `passwordRequestBlock` implementation should offer a way which allows the user to input a document password. When your implementation is ready the `providePassword` API call should be made. If the password is correct then the document will display, if incorrect then your application should handle the failure.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            doc.providePassword("password")


    .. tab:: Objective-C

        .. code-block:: obj-c

            [doc providePassword:@"password"];



Document actions
-----------------------

Undo
~~~~~~~~~~~

To undo a previous action (such as adding an annotation) use the the following method against your document instance.



.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            if doc.canUndo {
                doc.undo()
            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            if (doc.canUndo) {
                [doc undo];
            }

Redo
~~~~~~~~~~

To redo a previous action (such as adding an annotation) use the the following method against your document instance.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            if doc.canRedo {
                doc.redo()
            }


    .. tab:: Objective-C

        .. code-block:: obj-c

            if (doc.canRedo) {
                [doc redo];
            }



Get Selected Text
~~~~~~~~~~~~~~~~~~~~~~~~~

If the document has selected text (i.e. PDF document text which the user has selected) then this text can be read by your application with the selectedText method.


.. tabs::

    .. tab:: Swift

        .. code-block:: swift

            doc.selectedText({ text in
                print("text=\(text)")
            })


    .. tab:: Objective-C

        .. code-block:: obj-c

            [doc selectedText:^(NSString *text) {
                NSLog(@"text=%@",text);
            }];



.. note::

    Typically an application will want to copy the text value to the pasteboard.


.. include:: footer.rst