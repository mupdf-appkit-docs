.. include:: header.rst



Raster Image Export
=======================



An application developer can export a document's pages into raster image formats by invoking the MuPDF library directly, opening the document and processing the resulting pages into a raster image file sequence.

Loading MuPDF without a UI
-------------------------------------


To load the MuPDF library and use it directly an application developer should import `SODKLib` and request a document load as follows:



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.solib.*

        fun loadDocument(
                activity: Activity,
                path: String
            ) {
            // use the registered configuration options of the application as
            // the document configuration options
            val docCfg = ArDkLib.getAppConfigOptions()

            val lib = ArDkUtils.getLibraryForPath(activity, path)

            // Load the document
            val doc:ArDkDoc = lib.openDocument(path, object : SODocLoadListener {
                override fun onPageLoad(pageNum: Int) {

                }

                override fun onDocComplete() {
                    // see sample app
                    val pngTask = GeneratePngsTask()
                    pngTask.execute(null as Void?)
                }

                override fun onError(error: Int, errorNum: Int) {
                    // Called when the document load fails
                }

                override fun onSelectionChanged(
                    startPage: Int,
                    endPage: Int
                ) {
                    // Called when the selection changes
                }

                override fun onLayoutCompleted() {
                    // Called when a core layout is done
                }
            }, activity, docCfg)
        }


        private class GeneratePngsTask : AsyncTask<Void, Void, Boolean>() {
            // see sample app
            override fun doInBackground(vararg p0: Void?): Boolean {

            }
        }




   .. tab:: Java

      .. code-block:: java

        import com.artifex.solib.*;

        public void loadDocument(final Activity activity,
                                 final String path)
        {
            // use the registered configuration options of the application as
            // the document configuration options.
            ConfigOptions docCfg = ArDkLib.getAppConfigOptions();

            ArDkLib lib = ArDkUtils.getLibraryForPath(activity, path);

            // Load the document.
            ArDkDoc doc = lib.openDocument(path, new SODocLoadListener()
            {
                @Override
                public void onPageLoad(int pageNum)
                {

                }

                @Override
                public void onDocComplete()
                {
                    // see sample app
                    GeneratePngsTask pngTask = new GeneratePngsTask();
                    pngTask.execute((Void)null);

                }

                @Override
                public void onError(final int error, final int errorNum)
                {
                    // Called when the document load fails
                }

                @Override
                public void onSelectionChanged(final int startPage,
                                               final int endPage)
                {
                    // Called when the selection changes
                }

                @Override
                public void onLayoutCompleted()
                {
                    // Called when a core layout is done
                }
            }, activity, docCfg);
        }

        private class GeneratePngsTask extends AsyncTask<Void, Void, Boolean> {
            @Override
            protected Boolean doInBackground(Void... voids) {
                return null;
            }
        }




By using the document load listeners an application developer should be able use a background task to process document pages as required and use the `createBitmapForPath` method of the `SODKLib` to export pages in bitmap format.

See the :ref:`Android Sample app<App_Kit_Download>` and the `ExportFileAsPng` class to reference a full example.



.. include:: footer.rst