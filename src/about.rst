
.. include:: header.rst

.. _App_Kit_API:

About
========================


MuPDF SDK
-----------

MuPDF is a software framework for viewing and converting PDF, XPS, and E-book documents. It builds and runs on almost any OS you can imagine.

MuPDF App Kit
----------------------

The MuPDF App Kits build upon the SDK to offer simple code samples to enable you to build mobile Apps based upon MuPDF. The developer documentation outlines how to use these App Kits for both Android and iOS platforms.

Licensing
-----------

You are free to use the MuPDF App Kit for evaluation purposes. However, if you require to publish an application, then a commercial license is required.

For more on how to use your license key see:

- :ref:`Android License Key<Android_License_Key>`

- :ref:`iOS License Key<iOS_License_Key>`

Commercial license
~~~~~~~~~~~~~~~~~~~~~~~

A commercial license for MuPDF App Kit will be necessary before publishing your App project to an application store or other type of digital distribution platform for software applications. Please read the `Software License Terms and Conditions`_ document in its entirety.

.. include:: footer.rst

.. _Software License Terms and Conditions : https://artifex.com/mupdf-appkit/terms-conditions.html
