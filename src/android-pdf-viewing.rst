.. include:: header.rst



PDF Viewing
==================



Presenting a Document
----------------------------

There are two fundamental ways of presenting a document to the screen. One way is to use the :ref:`Default UI<Android_DefaultUI>` which includes an in-built user interface. The alternative is to load the document into a dedicated document view and provide your own :ref:`Custom UI<Android_CustomUI>` with listener methods for your document control.


.. _Android_DefaultUI:

Default UI
----------------------------


The :ref:`Default UI<Android_DefaultUI>` is an App Kit UI created by Artifex which includes a user-interface for typical document features and actions. It is presented at the top of the document view and accommodates for both tablet and phone layout.

The :ref:`Default UI<Android_DefaultUI>` aims to deliver a handy way of allowing for document viewing & manipulation without the need to provide your own :ref:`Custom UI<Android_CustomUI>`.


Instantiating the Default UI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


An application developer should import the `DefaultUIActivity` and instantiate it as follows:




.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.default_ui.DefaultUIActivity
        ...

        val defaultUI = Intent(this, DefaultUIActivity::class.java).apply {
            this.action = Intent.ACTION_VIEW
            this.data = uri
        }

        startActivity(defaultUI)


   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.default_ui.DefaultUIActivity;

        Intent defaultUI = new Intent(this, DefaultUIActivity.class);
        defaultUI.setAction(Intent.ACTION_VIEW);
        defaultUI.setData(uri);
        startActivity(defaultUI);




.. note::

    If you send `null` for the data `uri` value then the system file browser will open, whereby you can select a file to view.



.. _Android_CustomUI:

Custom UI
--------------


Providing a :ref:`Custom UI<Android_CustomUI>` means that the application developer is responsible for providing their own UI and functionality. This approach involves document presentation within an instance of DocumentView.

Considering that you have a valid document `Uri` from a `Uri File`_ instance, an application developer should initialize the document as explained in `Starting a Document View`_.




.. note::

    Don't forget to attach the DocumentView to your view hierarchy and set it's metrics to fit the area you need for display.




Starting a Document View
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A `DocumentView` is the central class which MuPDF uses for document display and is required to be imported and instantiated.

An application developer will typically set this up in their relevant `Activity` layout as follows:


.. code-block:: xml

    <com.artifex.sonui.editor.DocumentView
        android:id="@+id/doc_view"
        android:layout_width="fill_parent"
        android:layout_height="fill_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent">
    </com.artifex.sonui.editor.DocumentView>


The `start()` method for `DocumentView` allows 3 parameters as follows:

- uri: `Uri` The document URI.
- page: `Int` The document page to start viewing from (Note: if this value is out of bounds then the document will render to the nearest available page).
- showUI: `Bool` Indicates whether to render and use the :ref:`:ref:`Default UI<Android_DefaultUI>`<Android_DefaultUI>` on top of the document view or not - for the :ref:`:ref:`Custom UI<Android_CustomUI>`<Android_CustomUI>`  this should always be set to `false`.



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        import com.artifex.sonui.editor.DocumentView

        fun presentDocument(documentUri:Uri) {
            val documentView:DocumentView = findViewById(R.id.doc_view)
            documentView.start(documentUri, 0, false)
        }



   .. tab:: Java

      .. code-block:: java

        import com.artifex.sonui.editor.DocumentView;

        public void presentDocument(Uri documentUri) {
            DocumentView documentView = findViewById(R.id.doc_view);
            documentView.start(documentUri, 0, false);
        }



.. note::

    Don't forget that your `Activity` should also be responsible for setting up the `Activity` lifecycle interfaces previously explained.

    You should also adhere to the document listeners which allow for feedback against document events.



Going to a Page
~~~~~~~~~~~~~~~~~~

Once a document is loaded an application developer can view pages either by scrolling the document view or by using the App Kit API as follows:



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        // note: page number is zero-indexed,
        // thus this would show page 8 of your document
        documentView.goToPage(7)



   .. tab:: Java

      .. code-block:: java

        // note: page number is zero-indexed,
        // thus this would show page 8 of your document
        documentView.goToPage(7);


Jump to first page:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.firstPage()

   .. tab:: Java

      .. code-block:: java

        documentView.firstPage();


Jump to last page:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView.lastPage()

   .. tab:: Java

      .. code-block:: java

        documentView.lastPage();




In the code sample above `documentView` refers to the instance of your `DocumentView`. Furthermore this API should only be called after the document has initially loaded and had it's first render (see :ref:`Document Listeners - Document completed<Android_DocumentAPI_Listeners>`).




Viewing Full-screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to view a document in full-screen, it is up to the application developer to hide any UI which has been presented, and set the frame of the document view to fill the screen. Once that's done, calling enterFullScreen and passing a Runnable to it will invoke the full-screen mode. When the user taps to exit full-screen mode, the Runnable will be invoked, at which time the UI and frame should be restored to their previous state.


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        documentView?.enterFullScreen({
            // restore our UI
        })



   .. tab:: Java

      .. code-block:: java

        if (documentView != null) {
            documentView.enterFullScreen(new Runnable() {
                @Override
                public void run() {
                    // restore our UI
                }
            });
        }


Document Page Viewer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A handy way of showing or hiding the page navigator in your :ref:`Custom UI<Android_CustomUI>` can be utilized with the following methods:


.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        // show
        documentView?.showPageList()

        // hide
        documentView?.hidePageList()


   .. tab:: Java

      .. code-block:: java


        // show
        if (documentView != null) {
            documentView.showPageList();
        }

        // hide
        if (documentView != null) {
            documentView.hidePageList();
        }



You can also query the presence of the page list UI with:



.. tabs::

   .. tab:: Kotlin

      .. code-block:: kotlin

        val isVisible:Boolean? = documentView?.isPageListVisible

   .. tab:: Java

      .. code-block:: java

        boolean isVisible = documentView.isPageListVisible();






.. include:: footer.rst



.. _Uri File : https://developer.android.com/reference/android/net/Uri#fromFile(java.io.File)
