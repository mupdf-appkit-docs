.. include:: header.rst



Getting Started
==================



System requirements
-----------------------

- Xcode minimum SDK: 9
- iOS minimum version: 11.0

Adding the App Kit to your project
-------------------------------------

You should have been provided with `mupdfdk.xcframework` & `mupdf_default_ui.xcframework` files for your project. These files are collectively the :title:`MuPDF App Kit` and contain all the :title:`App Kit` code required for your projects.

The linked frameworks should be added to your project file system and then referenced in the Frameworks section in Xcode.

See the Xcode screenshot below for an example:


.. image:: images/ios/xcode-frameworks.png
   :align: center


*Xcode embedding frameworks*


.. note::

    If there is an compile time error which complains that the linked framework cannot be found ensure to validate your workspace in Xcode with:

    `Build Settings -> Build Options -> Validate Workspace = YES`

.. _iOS_License_Key:


License Key
------------------

To remove the MuPDF document watermark from your App, you will need to use a license key to activate App Kit.

To acquire a license key for your app you should:

#. Go to `artifex.com/appkit`_
#. Purchase your license(s)
#. In your application code add the API call to activate the license

.. note::

    App Kit does not require network connection to validate a license key and there is no server tracking or cloud logging involved with key validation.


Using your License Key
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have a license key for MuPDF App Kit, it will be bound to the App ID which you will have defined at the time of purchase. Therefore you should ensure that the App ID in your iOS project is correctly set. This is defined in the Bundle Identifier for the product `$(PRODUCT_BUNDLE_IDENTIFIER)`.

Once you have confirmed that your Bundle Identifier is correctly named, then call the following API early on in your application code:



.. tabs::

    .. tab:: Swift

       .. code-block:: swift

            import mupdfdk
            ...

            let key:String = "put your license key here"
            MuPDFDKDocumentViewController.unlockAppKit(key)


    .. tab:: Objective-C

       .. code-block:: obj-c

            #import "mupdfdk/mupdfdk.h"
            ...

            const char *key = "put your license key here";
            [MuPDFDKDocumentViewController unlockAppKit:@(key)];


.. note::

    The API call to set the license key should be made before you instantiate an App Kit `DocumentView`.



.. include:: footer.rst



.. _artifex.com/appkit : https://artifex.com/mupdf-appkit

